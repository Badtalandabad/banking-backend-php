<?php

declare(strict_types=1);

require __DIR__ . '/../application/Init/initScript.php';

$app = new \Core\Application();
$app->run();
