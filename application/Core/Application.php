<?php

namespace Core;

/**
 * Base class for the application.
 */
class Application
{
    /**
     * Runs application
     */
    public function run(): void
    {
        ob_start();
        try {
            $command = (new Routing\Router())->navigate();

            $apiGate = (new \ModuleGateFactory())->getApiGate();
            if ($command) {
                $status = $apiGate->processRequest($command);
                //TODO: check the status
            } else {
                $apiGate->respondError('Route not found', 404);
            }

        } catch (\Throwable $exception) {
            http_response_code(500);
            echo $exception->getMessage();
        }
        ob_end_flush();
    }
}