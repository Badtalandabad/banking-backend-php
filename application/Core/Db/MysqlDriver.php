<?php

namespace Core\Db;

class MysqlDriver
{
    private \mysqli $connection;

    /**
     * Returns db connection
     *
     * @return \mysqli
     */
    public function getConnection(): \mysqli
    {
        return $this->connection;
    }

    public function __construct($hostname, $username, $password, $database)
    {
        $this->connect($hostname, $username, $password, $database);
    }

    public function connect($hostname, $username, $password, $database)
    {
        $this->connection = new \mysqli($hostname, $username, $password, $database);
    }

    public function execute(string $query)
    {
        $this->connection->query($query);
    }
}