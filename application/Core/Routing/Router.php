<?php

namespace Core\Routing;

use Core\Commands\Command;

/**
 * Does API routing.
 */
class Router
{
    const ROUT_ROOT = 'api';
    const ROUT_API_VERSION_1 = 'v1';

    /**
     * Analyses URI and returns command to execute
     *
     * @return callable|null
     *
     * @throws \Exception
     */
    public function navigate(): ?callable
    {
        $uriString = trim($_SERVER['DOCUMENT_URI'], '/');

        $uri = explode('/', $uriString, 3);

        // domain.com/api/V123/some-route
        $root = $uri[0] ?? null;
        $apiVersion = $uri[1] ?? null;
        $routeRaw = $uri[2] ?? null;

        $errorMessage = '';
        if ($root !== self::ROUT_ROOT) {
           $errorMessage .= 'URI root is not "api", invalid route. ';
        }
        if (!$apiVersion) {
            $errorMessage .= 'API version is missing. ';
        }
        if (!$routeRaw) {
            $errorMessage .= 'API route is missing. ';
        }
        if ($errorMessage) {
            throw new \Exception($errorMessage);
        }

        $routes = match ($apiVersion) {
            self::ROUT_API_VERSION_1 => new \Routes\ApiV1Routes(),
            //TODO switch from exception to returning 404
            default => throw new \Exception('Not supported API version'),
        };

        return $routes->route($routeRaw);
    }
}