<?php

namespace Core\Routing;

interface RoutesInterface
{
    public function route(string $route): ?callable;
}