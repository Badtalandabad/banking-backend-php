<?php

namespace Modules\Api\Controllers;

use Core\ControllerAbstract;
use Modules\Api\ApiResponse;

class IndexController extends ControllerAbstract
{
    private function sendResponse(ApiResponse $response, int $status)
    {
        http_response_code($status);
        header('Content-Type: application/json');
        echo $response;
    }
    /**
     * @throws \Exception
     */
    public function processRequest(callable $command): bool
    {
        $data = $command();

        $response = new ApiResponse($data, true);

        $this->sendResponse($response, 200);

        return true;
    }

    public function systemError(string $errorMessage, int $status = 500): bool
    {
        $response = new ApiResponse(new \stdClass(), false, $errorMessage);

        $this->sendResponse($response, $status);

        return true;
    }
}