<?php

namespace Modules\Api;

/**
 *
 */
class ApiResponse
{
    /**
     * @var object
     */
    private object $data;

    /**
     * @var bool
     */
    private bool $success;

    /**
     * @var string
     */
    private string $message = '';


    /**
     * @param object $data
     * @param bool $success
     * @param string $message
     */
    public function __construct(object $data, bool $success, string $message = '')
    {
        $this->data = $data;
        $this->success = $success;
        $this->message = $message;
    }

    /**
     * @return string
     */
    public function toJson(): string
    {
        $object = new \stdClass();
        $object->message = $this->message;
        $object->status = $this->success;
        $object->data = $this->data;

        return json_encode($object);
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->toJson();
    }
}