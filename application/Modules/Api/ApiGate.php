<?php

namespace Modules\Api;

use Core\ModuleGateAbstract;
use Modules\Api\Controllers\IndexController;

class ApiGate extends ModuleGateAbstract
{
    /**
     * @throws \Exception
     */
    public function processRequest(callable $command): bool
    {
        return (new IndexController())->processRequest($command);
    }

    public function respondError(string $errorMessage, int $status = 500): bool
    {
        return (new IndexController())->systemError($errorMessage, $status);
    }
}