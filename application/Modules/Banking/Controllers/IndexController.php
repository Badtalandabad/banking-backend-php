<?php

namespace Modules\Banking\Controllers;

class IndexController extends \Core\ControllerAbstract
{
    public function webGetBanks(): \stdClass
    {
        $list = $this->getBanks();
        $response = new \stdClass();
        $response->bankList = $list;
        return $response;
    }

    public function webApply(): \stdClass
    {
        $loginId = (new \ModuleGateFactory())->getUsersGate();


        $response = new \stdClass();
        $response->loginId = $loginId;

        return $response;
    }

    public function getBanks(): array
    {
        return ['Something something bank', 'Simple bank', 'Pancake'];
    }
}