<?php

namespace Modules\Banking;

use Core\ModuleGateAbstract;
use Modules\Banking\Controllers\IndexController;

class BankingGate extends ModuleGateAbstract
{
    /**
     * BankingModule constructor.
     */
    public function __construct()
    {

    }

    public function getBanks(): array
    {
        return (new IndexController())->getBanks();
    }
}