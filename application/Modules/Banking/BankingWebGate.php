<?php

namespace Modules\Banking;

use Core\ModuleGateAbstract;
use Modules\Banking\Controllers\IndexController;

class BankingWebGate extends ModuleGateAbstract
{

    public function getBanks(): \stdClass
    {
        return (new IndexController())->webGetBanks();
    }

    public function apply(): \stdClass
    {
        return (new IndexController())->webApply();
    }
}