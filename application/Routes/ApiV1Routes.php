<?php

namespace Routes;

use Core\Routing\RoutesInterface;

/**
 * Routes for the API version 1
 */
class ApiV1Routes implements RoutesInterface
{
    /**
     * Analyses route, given in URI, returns command for dispatcher to execute.
     *
     * @param string $route Route part of the URI
     *
     * @return callable|null Command for API module to process the request
     */
    public function route(string $route): ?callable
    {
        $gateFactory = new \ModuleGateFactory();

        return match ($route) {
            'get-banks' => fn() => $gateFactory->getBankingWebGate()->getBanks(),
            'apply' => fn() => $gateFactory->getBankingWebGate()->apply(),
            default => null,
        };
    }
}