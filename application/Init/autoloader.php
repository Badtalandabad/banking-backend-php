<?php

spl_autoload_register(function ($className) {
    $namespaces = explode('\\', $className);
    $relativePath = implode('/', $namespaces) . '.php';
    require APP_PATH . '/' . $relativePath;
});