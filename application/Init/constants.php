<?php
/*
 * Defines base constants
 * */

define('BASE_PATH', dirname(__DIR__, 2));

const APP_PATH_RELATIVE = 'application';
const APP_PATH = BASE_PATH . '/' . APP_PATH_RELATIVE;

const LOG_DIR = BASE_PATH . '/logs';

const PUBLIC_PATH = BASE_PATH . '/public';
