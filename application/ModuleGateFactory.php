<?php

use Modules\Api\ApiGate;
use Modules\Banking\BankingGate;
use Modules\Banking\BankingWebGate;
use Modules\Users\UsersGate;


class ModuleGateFactory
{
    public function getApiGate(): ApiGate
    {
        return new ApiGate();
    }

    public function getUsersGate(): UsersGate
    {
        return new UsersGate();
    }
    
    public function getBankingWebGate(): BankingWebGate
    {
        return new BankingWebGate();
    }
    public function getBankingGate(): BankingGate
    {
        return new BankingGate();
    }
}